package cat.itb.backgroundmusic.album;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.backgroundmusic.JoJoService;
import cat.itb.backgroundmusic.R;



public class AlbumFragment extends Fragment implements AudioManager.OnAudioFocusChangeListener {


    @BindView(R.id.giorno)
    ImageView giorno;
    private AlbumViewModel mViewModel;
    MediaPlayer mediaPlayer;
    private AudioManager mAudioManager;
    private int length;

    public static AlbumFragment newInstance() {
        return new AlbumFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAudioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);

    }



    @Override
    public void onAudioFocusChange(int focusChange)
    {
        switch (focusChange)
        {
            case AudioManager.AUDIOFOCUS_GAIN:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                resume();
                break;
            case AudioManager.AUDIOFOCUS_LOSS:
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                length = mediaPlayer.getCurrentPosition();
                getActivity().stopService(new Intent(getContext(), JoJoService.class));

                break;
        }
    }

    private void resume() {
        mediaPlayer.seekTo(length);
        mediaPlayer.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAudioManager.abandonAudioFocus(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.album_fragment, container, false);
        
        
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        mediaPlayer = MediaPlayer.create(getContext(), R.raw.giornothemegoldenwind);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);
    }

    @OnClick({R.id.giorno,R.id.btnStop})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.giorno:
                getActivity().startService(new Intent(getContext(), JoJoService.class));
                break;
            case R.id.btnStop:
                getActivity().stopService(new Intent(getContext(), JoJoService.class));
                length = mediaPlayer.getCurrentPosition();
                break;


        }
    }

}
